import os
import json
from systemd_service import Service
from jinja2 import Environment, FileSystemLoader, select_autoescape


FILE_TEMPLATE = "test.j2"                       # Шаблон юнитфайла
STREAM_LIST = "stream_list.json"                # Файл для чтения 
SYSTEMD_LOCATION = "/etc/systemd/system/"       # Расположение unit файлов

def getStreamList(fileMame: str) -> dict:
    # Читает файл, Возвращает слооарь стрим
    with open(fileMame) as st:
        streams = json.load(st)
    return streams


def createTemplate(fileName: str):
    env = Environment(
        loader=FileSystemLoader('.'),
        autoescape=select_autoescape(['j2'])
        )
    return env.get_template(fileName)



def reloadService(serviceName: str):
    # Перезапуск сервиса
    daemon = Service(serviceName)
    daemon.reload()
    daemon.restart()

def removeService(serviceName):
    # Удаление сервиса
    daemon = Service(serviceName)
    daemon.stop()
    daemon.disable()  # Disable the unit.
    daemon.remove()
    daemon.reload()


def main():

    template = createTemplate(FILE_TEMPLATE)
    streamList = getStreamList(STREAM_LIST)

    # Цикл создает и запускает servise
    for ln in streamList.get('streams'):
        rendered_file = template.render(
            strem = ln
        )
        serviceName = str(ln.get("serviceName") + ".service")
        try:
            with open(os.path.join(SYSTEMD_LOCATION, serviceName), 'r') as file:
                text = file.read()
        except:
            text = ''
        # Сравниваем существующий unit файл c шаблоном, если они отличаются, обновляем югит файл
        if text != rendered_file:
            # Проверяем существование директории для сохранения hls
            if not os.path.exists(os.path.join(ln.get('folder'), ln.get('serviceName'))):
                print(f"Создаем папку {os.path.join(ln.get('folder'), ln.get('serviceName'))}")
                os.mkdir(os.path.join(ln.get('folder'), ln.get('serviceName')))
            with open(os.path.join(SYSTEMD_LOCATION, serviceName), 'w') as file:
                print(f"Создаем юнит файл {ln.get('serviceName')}")
                file.write(rendered_file)
            reloadService(ln.get("serviceName"))


    if os.path.exists("LastState.json"):

        try:

            newState = list()

            [newState.append(n.get("serviceName")) for n in streamList.get('streams')]

            for i in getStreamList("LastState.json").get('streams'):
                if i.get("serviceName") not in newState:
                    print(i.get("serviceName"))
                    removeService(i.get("serviceName"))
        except:
            print("Проверьте файлы")
    

    with open("LastState.json", 'w') as file:
        file.write(str(streamList).replace("'", '"'))

main()